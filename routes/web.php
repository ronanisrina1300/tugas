<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('home');
});

// Route::get('edulevels', 'EdulevelController@data');sama seperti yang bawah (catatan Saras)
Route::get('edulevels', [App\Http\Controllers\EdulevelController::class, 'data']);
Route::get('edulevels/add', [App\Http\Controllers\EdulevelController::class, 'add']);
Route::post('edulevels', [App\Http\Controllers\EdulevelController::class, 'addProcess']);
// Route::get('edulevels/edit/{id}', 'EdulevelController@edit'); sama seperti yang bawah (catatan Saras)
Route::get('edulevels/edit/{id}', [App\Http\Controllers\EdulevelController::class, 'edit']);
// Route::patch('edulevels/{id}', 'EdulevelController@editProcess'); sama seperti yang bawah (catatan Saras)
Route::patch('edulevels/{id}', [App\Http\Controllers\EdulevelController::class, 'editProcess']);
// Route::delete('edulevels/{id}', 'EdulevelController@delete'); sama seperti yang bawah (catatan Saras)
Route::delete('edulevels/{id}', [App\Http\Controllers\EdulevelController::class, 'delete']);