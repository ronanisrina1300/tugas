<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EdulevelController extends Controller
{
    public function data()
    {
    	$edulevels = \DB::table('edulevels')->get();
    	

    	//return $edulevels; 
    	//opsi pertama untuk menampilkan array yang gagal jalan, Rona comment di EdulevelSeeder.php bagian public function run (kalau ini outputnya array biasa gitu)

    	//dd($edulevels); 
		//opsi kedua untuk menampilkan array yang gagal jalan Rona comment di EdulevelSeeder.php bagian public function run (kalau ini outputnya array dengan background mirip text editor)

    	return view('edulevel.data', ['edulevels'=> $edulevels]);
    }

   public function add()
    {
    	
    	return view('edulevels.add');
    	
    }


    public function addProcess(Request $request)
    {
    	$request->validate([
			'name' => 'required|min:2',
			'desc' => 'required',
		],[
			'name.required' => 'Nama jenjang tidak boleh kosong' 
			//Untuk custom pesan peringatan di nama jenjang pada Tambah Jenjang (catatan Saras)
		]);

    	DB::table('edulevels')->insert([
    		'name' => $request->name, 
    		'desc' => $request->desc
    	]);
    	return redirect('edulevels')->with('status', 'Jenjang berhasil ditambah!');
    }

	public function edit($id)
	{
		$edulevel = DB::table('edulevels')->where('id', $id)->first();
		return view('edulevel/edit', compact('edulevel'));
	}

	public function editProcess(Request $request, $id)
	{
		$request->validate([
			'name' => 'required|min:2',
			'desc' => 'required',
		]);

		DB::table('edulevels')->where('id', $id)
			->update([
				'name' => $request->name, 
    			'desc' => $request->desc
			]);
		return redirect('edulevels')->with('status', 'Jenjang berhasil diupdate!');
	}
	
	public function delete($id)
	{
		DB::table('edulevels')->where('id', $id)->delete();
		return redirect('edulevels')->with('status', 'Jenjang berhasil dihapus!');
	}
}
